#include "EventLoop/Job.h"
#include "EventLoop/StatusCode.h"
#include "EventLoop/Worker.h"
#include "EventLoop/OutputStream.h"

//#include "RootCoreUtils/CheckRootVersion.hh"
//  RCU::disable_root_version_check()

//#include "CxAODMaker_VHbb/AnalysisBase_VHbb.h"
#include "CxAODMaker_Zemu/AnalysisBase_Zemu.h"


//#include "CxAODMaker/ElectronHandler.h"
#include "CxAODMaker_Zemu/ElectronHandler_Zemu.h"
//#include "CxAODMaker/MuonHandler.h"
#include "CxAODMaker_Zemu/MuonHandler_Zemu.h"
//#include "CxAODMaker/JetHandler.h"
#include "CxAODMaker_Zemu/JetHandler_Zemu.h"
#include "CxAODMaker/TauHandler.h"
#include "CxAODMaker/METHandler.h"
#include "CxAODMaker/TruthParticleHandler.h"
#include "CxAODMaker/EventInfoHandler.h"
#include "CxAODMaker/TruthEventHandler.h"

#include "CxAODMaker_Zemu/EventSelector_Zemu.h"
#include "CxAODTools_Zemu/EventSelection_Zemu.h"

#include "TFile.h"
#include "TTree.h"
#include "TTreeFormula.h"

#include "CxAODMaker/PhotonHandler.h"
#include "CxAODMaker/FatJetHandler.h"
#include "CxAODMaker/TrackJetHandler.h"
#include "CxAODMaker/TruthJetHandler.h"
#include "CxAODMaker/JetRegression.h"
#include "CxAODMaker/JetSemileptonic.h"
#include "CxAODTools/ParticleLinker.h"
#include "CxAODTools/OverlapRegisterAccessor.h"

#include "CxAODTools/OverlapRemoval.h"

// AnalysisContext
#include "CxAODTools/OvCompat.h"

/*
#include "CxAODMaker/EventSelector.h"

//#include "CxAODMaker_VHbb/ElectronHandler_VHbb.h"
//#include "CxAODMaker_VHbb/MuonHandler_VHbb.h"
//#include "CxAODMaker_VHbb/FatJetHandler_VHbb.h"
//#include "CxAODMaker_VHbb/JetHandler_VHbb.h"
#include "CxAODMaker/TauHandler.h"
//#include "CxAODMaker_VHbb/PhotonHandler_VHbb.h"
#include "CxAODMaker/TrackJetHandler.h"
#include "CxAODMaker/TruthJetHandler.h"
#include "CxAODMaker/METHandler.h"
#include "CxAODMaker/TruthParticleHandler.h"
#include "CxAODMaker/EventInfoHandler.h"
#include "CxAODMaker/TruthEventHandler.h"


#include "CxAODTools_VHbb/VHbb0lepEvtSelection.h"
#include "CxAODTools_VHbb/VHbb1lepEvtSelection.h"
#include "CxAODTools_VHbb/VHbb2lepEvtSelection.h"
#include "CxAODTools_VHbb/VBFHbbInclEvtSelection.h"
#include "CxAODTools_VHbb/VBFHbb1phEvtSelection.h"
*/




// this is needed to distribute the algorithm to the workers
ClassImp(AnalysisBase_Zemu)


EL::StatusCode AnalysisBase_Zemu::initializeHandlers() {

  /*
  if( AnalysisBase::initializeHandlers() != EL::StatusCode::SUCCESS ){
    Info("AnalysisBase_Zemu::initializeHandlers()", "AnalysisBase::initializeHandlers() returned a failure!");
    return EL::StatusCode::FAILURE;
  }
  */

  Info("AnalysisBase_Zemu::initializeHandlers()","Initialize handlers.");

  // initialize EventInfoHandler
  m_eventInfoHandler = new EventInfoHandler(*m_config, m_event);
  //pass information if running on derivation and MC / data to EventInfoHandler in order to have it available in the other object handlers
  m_eventInfoHandler->set_isMC(m_isMC);
  m_eventInfoHandler->set_isDerivation(m_isDerivation);
  m_eventInfoHandler->set_derivationName(m_derivationName);
  m_eventInfoHandler->set_isAFII(m_isAFII);
  EL_CHECK("AnalysisBase_Zemu::initializeHandlers()",m_eventInfoHandler->initialize());
    //bool do_MET = true;
  // note: the names of the handlers determine which collections are
  //       read, see also CxAODMaker-job.cfg

  // these have global pointer to be used e.g. in event selection
  // this is the place to define/register all the different/derivated handlers to the variable m_XXXHnadler.
  if (m_isMC) {
    m_truthParticleHandler = registerHandler<TruthParticleHandler>("truthParticle");
   }
  //m_muonHandler     = registerHandler<MuonHandler>("muon");
  m_muonHandler     = registerHandler<MuonHandler_Zemu>("muon");
  m_tauHandler      = registerHandler<TauHandler>("tau");
  //m_electronHandler = registerHandler<ElectronHandler>("electron");
  m_electronHandler = registerHandler<ElectronHandler_Zemu>("electron");
  m_photonHandler   = registerHandler<PhotonHandler>("photon");
  //m_jetHandler      = registerHandler<JetHandler>("jet");
  m_jetHandler      = registerHandler<JetHandler_Zemu>("jet");
  m_fatjetHandler   = registerHandler<FatJetHandler>("fatJet");
  m_trackjetHandler = registerHandler<TrackJetHandler>("trackJet");
  m_METHandler      = registerHandler<METHandler>("MET");
  /*
  //if (do_MET){
      m_METHandler_CST  = registerHandler<METHandler_LLP>("METCST");
      m_METHandler_TST  = registerHandler<METHandler_LLP>("METTST");
      m_METHandler      = registerHandler<METHandler>("MET");
  //}//end if
  */
  // these are spectators: they are calibrated and written to output,
  //                       but are not used in the event selection

  registerHandler<JetHandler>("jetSpectator");
  //if (do_MET) m_METTrackHandler = registerHandler<METHandler>("METTrack");
  m_METTrackHandler = registerHandler<METHandler>("METTrack");

  if (m_isMC) {
    registerHandler<METHandler>("METTruth");
    registerHandler<TruthEventHandler>("truthEvent");
  }

  // alternative: manual handler initialization (e.g. with different constructor)
  //  std::string containerName;
  //  std::string name = "muon";
  //  m_config->getif<std::string>(name + "Container", containerName);
  //  if ( ! containerName.empty() ) {
  //    m_muonHandler = new MuonHandler(name, *m_config, m_event, *m_eventInfoHandler);
  //    m_objectHandler.push_back( m_muonHandler );
  //  }

  // for tau truth matching
  if (m_tauHandler) {
    m_tauHandler->setTruthHandler(m_truthParticleHandler);
  }


  // Set trigger stream info if data
  // need to catch embedding once the time has come
  bool m_isEmbedding=false;
  if( !m_isMC && !m_isEmbedding ){
    TString sampleName = wk()->metaData()->castString("sample_name");
    if(sampleName.Contains("Egamma")) m_eventInfoHandler->set_TriggerStream(1);
    if(sampleName.Contains("Muons")) m_eventInfoHandler->set_TriggerStream(2);
    if(sampleName.Contains("Jet")) m_eventInfoHandler->set_TriggerStream(3);
  }


  return EL::StatusCode::SUCCESS;

} //end of the initializeHandlers() function




EL::StatusCode AnalysisBase_Zemu::initializeSelector() {

  
  if( AnalysisBase::initializeSelector() != EL::StatusCode::SUCCESS ){
    Info("AnalysisBase_Zemu::initializeSelector()", "AnalysisBase::initializeSelector() returned a failure!");
    return EL::StatusCode::FAILURE;
  }
  

  Info("initializeSelector()", "Initialize selector.");
  // initialize EventSelector
  m_selector = new EventSelector_Zemu(*m_config);

  //printf("\n here Initialize EventSelector_Zemu successfully  \n \n");

  m_selector->setJets(m_jetHandler);
  m_selector->setFatJets(m_fatjetHandler);
  m_selector->setTrackJets(m_trackjetHandler);
  m_selector->setMuons(m_muonHandler);
  m_selector->setTaus(m_tauHandler);
  m_selector->setElectrons(m_electronHandler);
  m_selector->setPhotons(m_photonHandler);
  m_selector->setMET(m_METHandler);
  //m_selector->setTruthParticles(m_truthParticleHandler);
  //((EventSelector_LLP*)m_selector)->setMET_TST(m_METHandler_TST);
  //((EventSelector_LLP*)m_selector)->setMET_CST(m_METHandler_CST);
  //((EventSelector_LLP*)m_selector)->setTruthParticles(m_truthParticleHandler);
  m_selector->setEventInfo(m_eventInfoHandler);
  //m_selector->setTruthEvent(m_truthEventHandler);

  EL_CHECK("EventSelector::initialize",m_selector->initialize());

  return EL::StatusCode::SUCCESS;
} //end initializeSelector function



EL::StatusCode AnalysisBase_Zemu::initializeTools() {

  if( AnalysisBase::initializeTools() != EL::StatusCode::SUCCESS ){
  	Info("AnalysisBase_LLP::initializeTools()", "AnalysisBase::initializeTools() returned a failure!");
  	return EL::StatusCode::FAILURE;
  }//end if : call the super() function
/*
  if (m_METHandler_TST) {
    m_METHandler_TST->setJets(m_jetHandler);
    m_METHandler_TST->setMuons(m_muonHandler);
    m_METHandler_TST->setTaus(m_tauHandler);
    m_METHandler_TST->setElectrons(m_electronHandler);
    m_METHandler_TST->setPhotons(m_photonHandler);

    if (m_isMC) {
      EL_CHECK("initializeTools()", m_METHandler_TST->addParticleVariations());
    }
  }//end if  : initialize METHandler_TST

  if (m_METHandler_CST) {
    m_METHandler_CST->setJets(m_jetHandler);
    m_METHandler_CST->setMuons(m_muonHandler);
    m_METHandler_CST->setTaus(m_tauHandler);
    m_METHandler_CST->setElectrons(m_electronHandler);
    m_METHandler_CST->setPhotons(m_photonHandler);

    if (m_isMC) {
      EL_CHECK("initializeTools()", m_METHandler_CST->addParticleVariations());
    }
  }//end if : initialize METHandler_CST
*/
  return EL::StatusCode::SUCCESS;
} //end initializeTools() function



EL::StatusCode AnalysisBase_Zemu::initializeSelection() {

  Info("initializeSelection()","...");

  // determine selection name

  std::string selectionName = "";

  bool autoDetermineSelection;
  m_config->getif<bool>("autoDetermineSelection", autoDetermineSelection);
  if (!autoDetermineSelection) {
    m_config->getif<std::string>("selectionName", selectionName);
  } 
  /*
  else {
    TString sampleName = wk()->metaData()->castString("sample_name");
    if      (sampleName.Contains("HIGG5D1")) selectionName = "0lep";
    else if (sampleName.Contains("HIGG5D2")) selectionName = "1lep";
    else if (sampleName.Contains("HIGG2D4")) selectionName = "2lep";
    else if (sampleName.Contains("HIGG5D3")) selectionName = "vbf";
    else {
      Error("initialize()", "Could not auto determine selection!");
      return EL::StatusCode::FAILURE;
    }
  }
  */

  // initialize event selection

  bool applySelection = false;
  m_config->getif<bool>("applyEventSelection", applySelection);
  if (applySelection) {
    Info("initialize()", "Applying selection: %s", selectionName.c_str());
	/*
    if      (selectionName == "0lep") m_selector->setSelection(new VHbb0lepEvtSelection());
    else if (selectionName == "1lep") m_selector->setSelection(new VHbb1lepEvtSelection());
    else if (selectionName == "2lep") m_selector->setSelection(new VHbb2lepEvtSelection());
    else if (selectionName == "vbf" ) m_selector->setSelection(new VBFHbbInclEvtSelection());
    else if (selectionName == "vbfa") m_selector->setSelection(new VBFHbb1phEvtSelection());
	*/
	if (selectionName == "Zemu") m_selector->setSelection(new EventSelection_Zemu()); // this is my analysis
    else {
      Error("initialize()", "Unknown selection requested!");
      return EL::StatusCode::FAILURE;
    }
  }

  // initialize overlap removal (possibly analysis specific)

  OverlapRemoval* overlapRemoval = new OverlapRemoval(*m_config);
  EL_CHECK("initializeSelection()",overlapRemoval->initialize());
  m_selector -> setOverlapRemoval(overlapRemoval);

  if (applySelection && (!m_muonHandler || !m_electronHandler || !m_jetHandler || !m_METHandler)) {
    Error("initialize()", "Not all collections for event selection are defined!");
    return EL::StatusCode::FAILURE;
  }

  return EL::StatusCode::SUCCESS;
} //end initializeSelection() function











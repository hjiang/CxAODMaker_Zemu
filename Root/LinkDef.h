#ifdef __CINT__

#include "CxAODMaker_Zemu/AnalysisBase_Zemu.h"

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#pragma link C++ class AnalysisBase_Zemu+;

#endif

#include "CxAODMaker_Zemu/EventSelector_Zemu.h"

#include "CxAODMaker/ObjectHandlerBase.h"
#include "CxAODTools/OverlapRegisterAccessor.h"
#include "CxAODTools/ReturnCheck.h"

#include <iterator>
#include <cstdlib>
#include <algorithm>
#include <vector>

EventSelector_Zemu::EventSelector_Zemu(ConfigStore& config) :
  EventSelector(config){
    //m_metTST = nullptr;
    //m_metCST = nullptr;
}//end Constructor


EL::StatusCode EventSelector_Zemu::fillSystematics() {

	/*
	if(EventSelector::fillSystematics(m_metTST) != EL::StatusCode::SUCCESS){
   		Error("EventSelector::fillSystematics()","Failed to fill systematics for TST! Exiting.");
    	return EL::StatusCode::FAILURE;
  	}//end if
	if(EventSelector::fillSystematics(m_metCST) != EL::StatusCode::SUCCESS){
   		Error("EventSelector::fillSystematics()","Failed to fill systematics for CST! Exiting.");
    	return EL::StatusCode::FAILURE;
  	}//end if
	*/

  	//Now call the super() function
  	return EventSelector::fillSystematics();
}//end fillSystematics() function

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// in the execute() function of most dafault file AnalysisBase.cxx, m_selector->performSelection(bool) is called
// while inside EventSelector.cxx, EventSelector::performSelection(bool& pass) is defined
// and inside EventSelector::performSelection(bool& pass), 
// call the function EventSelector::passORandPreSel(const TString& sysName) to handle the performSelection by looping variations
// (and "nominal" is included inside the variable "sysName")
// attention here EventSelector::performSelection(bool& pass) is not EventSelector::performSelection(const TString& sysName)
// (it's overloaded function, not overridden)
// while default EventSelector::performSelection(const TString& sysName) is defined inside EventSelector.cxx
// But here, it is overriden by my own EventSelector_Zemu::performSelection(const TString& sysName)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool EventSelector_Zemu::performSelection(const TString& sysName) {
/*
  if(m_debug) Info("EventSelector_Zemu::performSelection()", "Performing selection for variation '%s'." , sysName.Data());

  SelectionContainers containers;

  //bool applyMETAfterSelection(false);
  //m_config.getif<bool>("applyMETAfterSelection",applyMETAfterSelection);

  // first, retrieve the containers
  if ( m_jets      ) { containers.jets      = m_jets->getInParticleVariation(sysName)      ; }
  if ( m_fatjets   ) { containers.fatjets   = m_fatjets->getInParticleVariation(sysName)   ; }
  if ( m_trackjets ) { containers.trackjets = m_trackjets->getInParticleVariation(sysName) ; }
  if ( m_electrons ) { containers.electrons = m_electrons->getInParticleVariation(sysName) ; }
  if ( m_photons   ) { containers.photons   = m_photons->getInParticleVariation(sysName)   ; }
  if ( m_muons     ) { containers.muons     = m_muons->getInParticleVariation(sysName)     ; }
  if ( m_taus      ) { containers.taus      = m_taus->getInParticleVariation(sysName)      ; }
  if ( m_met       ) { containers.met       = m_met->getMET(sysName)   					   ; }
  //if ( m_met && !applyMETAfterSelection) { containers.met       = m_met->getMET(sysName)   ; }
  //if ( m_metMJTight && !applyMETAfterSelection) { containers.metmjtight = m_metMJTight->getMET(sysName); }
  //if ( m_metMJMiddle && !applyMETAfterSelection) { containers.metmjmiddle = m_metMJMiddle->getMET(sysName); }
  //if ( m_metMJLoose && !applyMETAfterSelection) { containers.metmjloose = m_metMJLoose->getMET(sysName); }
  //if ( m_metTrack && !applyMETAfterSelection) { containers.mettrack = m_metTrack->getMET(sysName); }

  //if ( m_truthParticles ) { containers.truthParticles = m_truthParticles->getInParticleVariation(sysName); }
  //if ( m_truthElectrons  ) { containers.truthElectrons = m_truthElectrons->getInParticleVariation(sysName); }
  //if ( m_truthMuons  ) { containers.truthMuons = m_truthMuons->getInParticleVariation(sysName); }
  //if ( m_truthNeutrinos  ) { containers.truthNeutrinos = m_truthNeutrinos->getInParticleVariation(sysName); }

  ///ADDED BY M. WERNER
  //if ( m_metCST && !applyMETAfterSelection) { containers.metcst = m_metCST->getMET(sysName); }
  //if ( m_metTST && !applyMETAfterSelection) { containers.mettst = m_metTST->getMET(sysName); }
  ///DONE ADDING BY M. WERNER


  if(m_debug) Info("EventSelector::performSelection()","Successfully retrieved InParticleVariations." );


  // at the moment no systematics for METTrack. Will hopefully change.
  // --> not used yet - will ned to update SelectionContainer when this is to be used!
  //if( m_metTrack ) { containers.mettrack = m_metTrack->getMET(); }

  // event info is required !
  if(! m_info) {
    Error("EventSelector::performSelection",
          "We need to know about EventInfo ! Please call setEventInfo before any attempt to apply selections");
    exit(EXIT_FAILURE);
  }

  // input event info for selection
  containers.evtinfo = m_info->getEventInfo();

  m_selection->setSysName(sysName);

  //printf("\n \n Here, is my own EventSelector_Zemu::performSelection \n \n \n");

  bool skipCutFlow = (sysName != "Nominal");
  // presence of m_selection has been checked in performSelection(bool)
  // this is the palce to call the fucntion passPreSelection() in the EventSelection_Zemu.cxx
  // here set the bool in EventSelection_Zemu::passPreSelection(SelectionContainers & containers, bool isKinVar)  will enable the cutflow, 
  // because in the passPreSelection(), the cutflow will be available when !isKinVar is true
  return m_selection->passPreSelection(containers, skipCutFlow);
*/

  return EventSelector::performSelection(sysName);
}//end performSelection(...) function



EL::StatusCode EventSelector_Zemu::fillEventVariables() {
/*
  if(m_debug) Info("EventSelector::fillEventVariables()","Performing selection.");

  if(m_isFirstCall) {
    if(fillSystematics() != EL::StatusCode::SUCCESS){
      Error("EventSelector::fillEventVariables()","Failed to fill systematics! Exiting.");
      return EL::StatusCode::FAILURE;
    }
  }

  if(!m_selection) {
    if(m_isFirstCall) {
      Warning("EventSelector::fillEventVariables()", "No event preselection has been set ! no event-vars will be written");
    }
  }

  m_isFirstCall = false;

  if(!m_selection)  return true;

  for(const std::string& sysName : m_systematics) {
    SelectionContainers containers;

    // first, retrieve the containers
    if ( m_jets      ) { containers.jets      = m_jets->getInParticleVariation(sysName)      ; }
    if ( m_fatjets   ) { containers.fatjets   = m_fatjets->getInParticleVariation(sysName)   ; }
    if ( m_trackjets ) { containers.trackjets = m_trackjets->getInParticleVariation(sysName) ; }
    if ( m_electrons ) { containers.electrons = m_electrons->getInParticleVariation(sysName) ; }
    if ( m_photons   ) { containers.photons   = m_photons->getInParticleVariation(sysName)   ; }
    if ( m_muons     ) { containers.muons     = m_muons->getInParticleVariation(sysName)     ; }
    if ( m_taus      ) { containers.taus      = m_taus->getInParticleVariation(sysName)      ; }
    if ( m_met       ) { containers.met       = m_met->getMET(sysName)                       ; }
    //if ( m_metMJTight) { containers.metmjtight= m_metMJTight->getMET(sysName)                ; }
    //if ( m_metMJMiddle) { containers.metmjmiddle= m_metMJMiddle->getMET(sysName)             ; }
    //if ( m_metMJLoose) { containers.metmjloose= m_metMJLoose->getMET(sysName)                ; }
    //if ( m_metTrack) { containers.mettrack= m_metTrack->getMET(sysName)                      ; }
    ///ADDDED BY M. WERNER
    //std::cout << "EVENT SELECTOR STORING MET" << std::endl;
    //if( !m_metCST ) std::cerr << "Event Selector Doesn't have an initialized CST Handler." << std::endl;

    //if ( m_metCST ) { containers.metcst = m_metCST->getMET(sysName); }
    //if ( m_metTST ) { containers.mettst = m_metTST->getMET(sysName); }
    ///DONE ADDING BY M. WERNER



    if(m_debug) Info("EventSelector::fillEventVariables()","Successfully retrieved InParticleVariations." );

    //TODO  at the moment no systematics for METTrack. Will hopefully change.
    // --> not used yet - will ned to update SelectionContainer when this is to be used!
    //if( m_metTrack ) { containers.mettrack = m_metTrack->getMET(); }

    // event info is required !
    if(! m_info) {
      Error("EventSelector::fillEventVariables",
            "We need to know about EventInfo ! Please call setEventInfo before any attempt to fill event-variables");
      exit(EXIT_FAILURE);
    }

    // input event info for selection
    containers.evtinfo =  m_info->getEventInfo();

    m_selection->setSysName(sysName);
    m_selection->passPreSelection(containers, true);

    // is it a variation?
    bool isKinVar = (sysName != "Nominal");
    bool isWeightVar = false;
    for(unsigned int iWeightVar=0; iWeightVar<m_weightVariations.size() ; ++iWeightVar ){
      if( sysName == m_weightVariations[iWeightVar]) {
        isWeightVar = true;
        isKinVar = false;
      }
    }

    // write event variables for variations that have a corresponding event info
    xAOD::EventInfo* evtinfoOut = m_info->getOutEventInfoVariation(sysName, false);
    if (evtinfoOut) {
      if (m_debug) {
        Info("EventSelector::fillEventVariables()", "Writing event variables.");
      }
      EL_CHECK("EventSelector::fillEventVariables()", m_selection->writeEventVariables(containers.evtinfo, evtinfoOut, isKinVar, isWeightVar, sysName, m_info->get_PileupRdmRun(), m_trig_sfmuon));
    }
  }
  return EL::StatusCode::SUCCESS;
*/

return EventSelector::fillEventVariables();
}







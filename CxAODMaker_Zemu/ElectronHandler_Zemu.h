// Dear emacs, this is -*-c++-*-
#ifndef CxAODMaker_ElectronHandler_Zemu_H
#define CxAODMaker_ElectronHandler_Zemu_H

#include "CxAODMaker/ElectronHandler.h"
#include "CxAODTools/CommonProperties.h"
#include "CxAODMaker/TruthParticleHandler.h"


class ElectronHandler_Zemu : public ElectronHandler {

public:

  ElectronHandler_Zemu(const std::string& name, ConfigStore & config, xAOD::TEvent * event,
                  EventInfoHandler & eventInfoHandler);
  ~ElectronHandler_Zemu();


protected:
  
  // selection functions
  //bool passVHLooseElectron(xAOD::Electron * electron);
  //bool passVHSignalElectron(xAOD::Electron * electron);//common cuts for ZH and WH
  //bool passZHSignalElectron(xAOD::Electron * electron);
  //bool passWHSignalElectron(xAOD::Electron * electron);
  //bool passWHSignalElectronIso(xAOD::Electron * electron);
  bool passPreSel(xAOD::Electron * electron);

  //Override function (calls base)
  virtual EL::StatusCode writeCustomVariables(xAOD::Electron* inElectron, xAOD::Electron* outElectron, 
                                      bool isKinVar, bool isWeightVar, const TString& sysName) override;
  //bool m_isMJ;

};

#endif






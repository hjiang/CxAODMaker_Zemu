// Dear emacs, this is -*-c++-*-

#ifndef CxAODMaker_MuonHandler_Zemu_H
#define CxAODMaker_MuonHandler_Zemu_H

#include "CxAODMaker/MuonHandler.h"

class MuonHandler_Zemu : public MuonHandler {

public:

  MuonHandler_Zemu(const std::string& name, ConfigStore & config, xAOD::TEvent * event,
              EventInfoHandler & eventInfoHandler);
  ~MuonHandler_Zemu();

protected:
  
  // selection functions
  //bool passVHLooseMuon(xAOD::Muon * part);
  //bool passVHSignalMuon(xAOD::Muon * part);//common cuts for ZH and WH
  //bool passZHSignalMuon(xAOD::Muon * part);
  //bool passWHSignalMuon(xAOD::Muon * part);
  //bool passWHSignalMuonIso(xAOD::Muon * part);
  bool passPreSel(xAOD::Muon* muon); 
 
  virtual EL::StatusCode writeCustomVariables(xAOD::Muon * inMuon, xAOD::Muon * outMuon, bool isKinVar, bool isWeightVar, const TString& sysName) override;
  //bool m_isMJ;

};

#endif

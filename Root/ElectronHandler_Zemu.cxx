// electron specific includes
#include "CxAODMaker_Zemu/ElectronHandler_Zemu.h"


// only its own handler constructor function is called, so all the functions inside it should be added to m_selectFcns to get called
ElectronHandler_Zemu::ElectronHandler_Zemu(const std::string& name, ConfigStore& config,
                                           xAOD::TEvent* event,
                                           EventInfoHandler& eventInfoHandler)
  : ElectronHandler(name, config, event, eventInfoHandler){

  //VHbb selector functions
  using std::placeholders::_1;
  m_selectFcns.clear();
  //m_selectFcns.push_back(std::bind( &ElectronHandler_VHbb::passVHLooseElectron, this, _1));
  //m_selectFcns.push_back(std::bind( &ElectronHandler_VHbb::passVHSignalElectron, this, _1));
  //m_selectFcns.push_back(std::bind( &ElectronHandler_VHbb::passZHSignalElectron, this, _1));
  //m_selectFcns.push_back(std::bind( &ElectronHandler_VHbb::passWHSignalElectron, this, _1));
  m_selectFcns.push_back(std::bind(&ElectronHandler_Zemu::passPreSel, this, _1)); 
}

ElectronHandler_Zemu::~ElectronHandler_Zemu(){
}

bool ElectronHandler_Zemu::passPreSel(xAOD::Electron * electron) 
{

  bool passSel = true;

  // here to set all the electrons pass passPreSel, and do the OverlapRemoval
  Props::passPreSel.set(electron, passSel);

  // then we make the cutflow of electron object, apply all the cuts on electrons
  // set priority of first cut to some value, which should be different for
  // other selection functions
  // cut names have to be unique (no check currently!)
  if (passSel) m_cutflow->count("Zemu Electron input", 100);


  if (!(electron->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON))) passSel = false;
  if (passSel) m_cutflow->count("isGoodOQ");
  Props::isGoodOQ.set(electron, passSel);

 
  // author cut not needed anymore since forward electrons are stored in a different container
  //if (!(electron->author() == xAOD::EgammaParameters::AuthorElectron)) passSel = false;

 
  //if(electron->caloCluster()){ 
  //  if (!(fabs(electron->caloCluster()->etaBE(2)) < 2.47)) passSel = false; //do we want to exclude the crack region?
  //}
  //else{
  if (!(fabs(electron->eta()) < 2.47) || (fabs(electron->eta()) > 1.37 && fabs(electron->eta()) < 1.52) )  passSel = false; //do we want to exclude the crack region?
    //Warning("ElectronHandler_Zemu::passPreSel","Did not find caloCluster, use eta() instead of caloCluster()->etaBE(2) to check eta range!");
  //}
  
  if (passSel) m_cutflow->count("|eta|");
  
  //if (!(electron->pt() > 7000.)) passSel = false;
  if (!(electron->pt() > 25000.)) passSel = false;
  if (passSel) m_cutflow->count("pt 25GeV");
  if (!(fabs(Props::d0sigBL.get(electron)) < 5)) passSel = false;
  if (passSel) m_cutflow->count("|d0sigBL|");
  if (!(fabs(Props::z0sinTheta.get(electron)) < 0.5)) passSel = false;
  if (passSel) m_cutflow->count("|z0sinTheta|");

  //if (!(Props::isLooseLH.get(electron))) passSel = false;
  if (!(Props::isMediumLH.get(electron))) passSel = false;
  if (passSel) m_cutflow->count("isMediumLH");
   
  // no isolation cut until isolation problems are fixed in input!
  //  if (!(electron->isolationValue(xAOD::Iso::ptcone20) / electron->pt() < 0.1)) passSel = false; 
  //if (passSel) m_cutflow->count("track iso loose");

  //if (!(Props::isLooseTrackOnlyIso.get(electron)) ) passSel = false;
  if (!(Props::isGradientIso.get(electron)) ) passSel = false;
  if (passSel) m_cutflow->count("Pass isGradientIso"); //Test of new isolation WP

  //Props::isVHLooseElectron.set(electron, passSel);
  Props::passPreSel.set(electron, passSel);
  Props::forMETRebuild.set(electron, passSel);//Using this for MET rebuilding
  if (passSel) m_cutflow->count("SignalElectron selected");

  return passSel;

}

EL::StatusCode ElectronHandler_Zemu::writeCustomVariables(xAOD::Electron * inElectron, xAOD::Electron * outElectron, bool /*isKinVar*/, bool isWeightVar, const TString&) 
{

  if(!isWeightVar) {
    Props::isGoodOQ.copy(inElectron, outElectron);
    Props::passOR_forReader.copy(inElectron, outElectron);
	Props::passORGlob_forReader.copy(inElectron, outElectron);
    //Props::isVHLooseElectron.copy(inElectron, outElectron);
    //Props::isZHSignalElectron.copy(inElectron, outElectron);
    //Props::isWHSignalElectron.copy(inElectron, outElectron);
    //outElectron->setIsolationValue(inElectron->isolationValue(xAOD::Iso::topoetcone30),xAOD::Iso::topoetcone30);
    //outElectron->setIsolationValue(inElectron->isolationValue(xAOD::Iso::topoetcone40),xAOD::Iso::topoetcone40);
  }

  return EL::StatusCode::SUCCESS;
}









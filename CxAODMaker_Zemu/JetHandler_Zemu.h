// Dear emacs, this is -*-c++-*-
#ifndef CxAODMaker_JetHandler_Zemu_H
#define CxAODMaker_JetHandler_Zemu_H

#include "CxAODMaker/JetHandler.h"

class JetHandler_Zemu : public JetHandler {

public:

  JetHandler_Zemu(const std::string& name, ConfigStore & config, xAOD::TEvent * event,
             EventInfoHandler & eventInfoHandler);
  ~JetHandler_Zemu();

protected:
  
  // selection functions
  bool passPreSelection(xAOD::Jet* jet);
  //bool passVetoJet(xAOD::Jet* jet);
  //bool passSignalJet(xAOD::Jet* jet);
  //bool checkCentralJet(xAOD::Jet* jet,bool isInCutFlow=0);

  //float m_btagCut;

  virtual EL::StatusCode writeCustomVariables(xAOD::Jet * inJet, xAOD::Jet * outJet, bool isKinVar, bool isWeightVar, const TString& sysName) override;
};

#endif

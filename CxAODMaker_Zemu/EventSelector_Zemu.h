#ifndef CxAODMaker_EventSelector_Zemu_H
#define CxAODMaker_EventSelector_Zemu_H

// infrastructure includes
#include "CxAODMaker/EventSelector.h"

class EventSelector_Zemu : public EventSelector {

protected:

	//METHandler* m_metTST;
	//METHandler* m_metCST;
    TruthParticleHandler* m_truthParticles;


  	virtual EL::StatusCode fillSystematics() override;
  	virtual bool performSelection(const TString& sysName) override;

public:

  	EventSelector_Zemu() = delete;
  	EventSelector_Zemu(ConfigStore & config);

  	//void setMET_CST(METHandler* met) { m_metCST = met; }
  	//void setMET_TST(METHandler* met) { m_metTST = met; }
    void setTruthParticles(TruthParticleHandler* truth) { m_truthParticles = truth; }
  	EL::StatusCode fillEventVariables() override;

protected :

};

#endif




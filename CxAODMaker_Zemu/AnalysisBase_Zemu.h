#ifndef CxAODMaker_AnalysisBase_Zemu_H
#define CxAODMaker_AnalysisBase_Zemu_H

#include "CxAODMaker/AnalysisBase.h"

class AnalysisBase_Zemu : public AnalysisBase {

protected:
  virtual EL::StatusCode initializeHandlers() override;
  virtual EL::StatusCode initializeSelector() override;
  virtual EL::StatusCode initializeTools() override;
  virtual EL::StatusCode initializeSelection() override;

public:
  AnalysisBase_Zemu()
    : AnalysisBase() {}

  ~AnalysisBase_Zemu() {}

  ClassDefOverride(AnalysisBase_Zemu, 1);
};

#endif






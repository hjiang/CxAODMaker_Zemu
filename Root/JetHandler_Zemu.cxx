#include <iostream>

// jet specific includes
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "CxAODMaker_Zemu/JetHandler_Zemu.h"

JetHandler_Zemu::JetHandler_Zemu(const std::string& name, ConfigStore & config, xAOD::TEvent * event,
                       EventInfoHandler & eventInfoHandler) :
  JetHandler(name, config, event, eventInfoHandler)
{
  using std::placeholders::_1;
  m_selectFcns.clear();
  m_selectFcns.push_back(std::bind(&JetHandler_Zemu::passPreSelection, this, _1));
  //m_selectFcns.push_back(std::bind( &JetHandler_VHbb::passVetoJet, this, _1));
  //m_selectFcns.push_back(std::bind( &JetHandler_VHbb::passSignalJet, this, _1));

  //m_config.getif<float>("btagCut", m_btagCut);
  
}

JetHandler_Zemu::~JetHandler_Zemu()
{
}

bool JetHandler_Zemu::passPreSelection(xAOD::Jet* jet)
{
  bool passSel = true;
  Props::passPreSel.set(jet, passSel);
  
  if (passSel) m_cutflow->count("Jet input", 300);

  if (jet->pt() < 20000.) passSel = false;
  if (passSel) m_cutflow->count("pt cut");

  if (fabs(jet->eta()) > 2.5) passSel = false;
  if (passSel) m_cutflow->count("#eta cut");

  Props::passPreSel.set(jet, passSel);
  // JVT TWiki with recommendation: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/JVTCalibration
  /*
  if(Props::PassJvtMedium.exists(jet)) {
    if(!(Props::PassJvtMedium.get(jet))) passSel = false;
  } else {
    if ((Props::Jvt.get(jet)) < 0.59
	&& jet->pt() < 60000. && fabs(jet->eta()) < 2.4 ) passSel = false;
  }
  */

  bool m_applyJVTcut = true;
  if (m_applyJVTcut) {
    if (Props::Jvt.get(jet) < 0.59 && jet->pt() < 50000. && fabs(jet->eta()) < 2.4)
      passSel = false;
  }
  if (passSel) m_cutflow->count("JVT cut");


  // passPreSel must be set BEFORE the goodJet requirement
  //Props::passPreSel.set(jet, passSel);

  if (!(Props::goodJet.get(jet))) passSel = false;
  if (passSel) m_cutflow->count("GoodJet");

  return passSel;
}

EL::StatusCode JetHandler_Zemu::writeCustomVariables(xAOD::Jet * inJet, xAOD::Jet * outJet, bool /*isKinVar*/, bool isWeightVar, const TString&)
{

  if(!isWeightVar) {
    Props::passOR_forReader.copy(inJet, outJet);
    Props::passORGlob_forReader.copy(inJet, outJet);
  }

  return EL::StatusCode::SUCCESS;
}



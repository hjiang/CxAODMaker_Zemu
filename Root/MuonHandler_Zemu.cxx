#include <iostream>

//muon specific includes
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "CxAODMaker_Zemu/MuonHandler_Zemu.h"
#include "MuonMomentumCorrections/MuonCalibrationAndSmearingTool.h"
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "MuonEfficiencyCorrections/MuonEfficiencyScaleFactors.h"

MuonHandler_Zemu::MuonHandler_Zemu(const std::string& name, ConfigStore & config, xAOD::TEvent *event,
    EventInfoHandler & eventInfoHandler) :
  MuonHandler(name, config, event, eventInfoHandler){


  using std::placeholders::_1;
  m_selectFcns.clear();
  //m_selectFcns.push_back(std::bind( &MuonHandler_VHbb::passVHLooseMuon, this, _1));
  //m_selectFcns.push_back(std::bind( &MuonHandler_VHbb::passVHSignalMuon, this, _1));
  //m_selectFcns.push_back(std::bind( &MuonHandler_VHbb::passZHSignalMuon, this, _1));
  //m_selectFcns.push_back(std::bind( &MuonHandler_VHbb::passWHSignalMuon, this, _1));
  m_selectFcns.push_back(std::bind(&MuonHandler_Zemu::passPreSel, this, _1));

}

MuonHandler_Zemu::~MuonHandler_Zemu(){
}

bool MuonHandler_Zemu::passPreSel(xAOD::Muon * muon) 
{
  bool passSel = true;
  Props::passPreSel.set(muon, passSel);

  if (passSel) m_cutflow->count("Muon input", 100);

  //apply selection for combined or segment tagged muons for now

  if (!(Props::acceptedMuonTool.get(muon)) ) passSel = false;
  if (passSel) m_cutflow->count("IsAtLeastLooseMuon");//This is the starting quality we require in the tool

  //if (!(muon->pt() > 7000.)) passSel = false;
  if (!(muon->pt() > 25000.)) passSel = false;
  if (passSel) m_cutflow->count("pt 25GeV");
 
  if (!(Props::muonQuality.get(muon) < 2)) passSel = false; // 0 == tight, 1 == medium. Selects medium & tight muons
  if (passSel) m_cutflow->count ("Muon quality");

  if (!(fabs(muon->eta()) < 2.5)) passSel = false;
  if (passSel) m_cutflow->count("|eta|<2.5 signal");

  if (!(fabs(Props::d0sigBL.get(muon)) < 3)) passSel = false;
  if (passSel) m_cutflow->count("|d0sigBL|");
  if (!(fabs(Props::z0sinTheta.get(muon)) < 0.5)) passSel = false;
  if (passSel) m_cutflow->count("|z0sinTheta|");
  //if (!(Props::isLooseTrackOnlyIso.get(muon)) ) passSel = false;
  if (!(Props::isGradientIso.get(muon)) ) passSel = false;
  if (passSel) m_cutflow->count("Pass isGradientIso"); //Test of new isolation WP
  
  // outdated cuts
  //---------------
  // Do not use author directly.  For 13TeV it will kill MuGirl(Combined) and MuTagIMO(SegmentTagged).
  // For 8TeV it makes no difference as only combined available is MuidCo and all MuTagIMO(SegmentTagged)muons fail passedIDCuts
  // if (!(muon->author() == xAOD::Muon::MuidCo)) passSel = false;
  // if (!(fabs(muon->eta()) < 2.7)) passSel = false; //done by the muon selection tool.
  // if (!(Props::passedIDCuts.get(muon))) passSel = false; //done by the muon selection tool.
  // Do not use quality directly. It always returns Loose==2(Tight==0) in 13(8)TeV. Quality selection(medium) is done by the muon selection tool.
  // if (!(muon->quality() == xAOD::Muon::Tight)) passSel = false;
  //float trackIso = -999.;
  //muon->isolation(trackIso,xAOD::Iso::ptcone20); 
  //  if (!((trackIso / muon->pt()) < 0.1)) passSel = false; //just for now until isolation problems are fixed in input!


  //Props::isVHLooseMuon.set(muon, passSel);
  Props::passPreSel.set(muon, passSel);
  Props::forMETRebuild.set(muon, passSel);//Using this for MET rebuilding
  if (passSel) m_cutflow->count("SignalMuon selected");
  return passSel;
}

EL::StatusCode MuonHandler_Zemu::writeCustomVariables(xAOD::Muon * inMuon, xAOD::Muon * outMuon, bool /*isKinVar*/, bool isWeightVar, const TString&)
{

  if(!isWeightVar) {
    Props::passOR_forReader.copy(inMuon, outMuon);
    Props::passORGlob_forReader.copy(inMuon, outMuon);
  }

  return EL::StatusCode::SUCCESS;
}
